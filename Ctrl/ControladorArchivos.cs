﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CtrlAdminArchivos.Ctrl
{
    public class ControladorArchivos
    {
        static List<FileInfo> files = new List<FileInfo>();  
        static readonly List<DirectoryInfo> folders = new List<DirectoryInfo>();

        public static List<string> ObtenerListaUnidadesEquipo()
        {
            DriveInfo[] listaUnidades = DriveInfo.GetDrives();

            return listaUnidades.Select(d => d.Name).ToList();
        }

        public static List<FileInfo> ObtenerArchivosSinExtension(string unidad)
        {
            files = new List<FileInfo>();
            DirectoryInfo di= new DirectoryInfo(unidad);
            ObtenerArchivos(di, "*");

            return files;
        }

        static void ObtenerArchivos(DirectoryInfo dir, string patronBusqueda)
        {            
            try
            {
                foreach (FileInfo arch in dir.GetFiles(patronBusqueda))
                {                    
                    if (string.IsNullOrEmpty(arch.Extension))
                        files.Add(arch);
                }
            }
            catch
            {                
             
            }

            try
            {

            foreach (DirectoryInfo folder in dir.GetDirectories())
            {
                folders.Add(folder);
                ObtenerArchivos(folder, patronBusqueda);
            }
            }
            catch
            {                
            }
        }

    }
}
