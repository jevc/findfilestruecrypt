﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CtrlAdminArchivos.Ctrl;

namespace FindFilesTrueCrypt.Ctrl
{
    public class Controlador
    {

        #region Atributos
        #endregion
        #region Constructores
        #endregion
        #region Propiedades
        #endregion
        #region Metodos

        /// <summary>
        /// Método que permite obtener los archivos sin extensión
        /// </summary>
        /// <param name="unidad">Cadena que contiene la unidad en la que se realizará la búsqueda</param>
        /// <returns>Cadena de texto que contiene todos los archivos localizados</returns>
        public string ObtenerArchivosTrueCrypt(string unidad)
        {            
            // invoca la consulta de los archivos que no tienen extensión
            List<FileInfo> listaArch = ControladorArchivos.ObtenerArchivosSinExtension(unidad);

            // crea y retorna una nueva cadena con la ruta y nombres de los archivos localizados
            return listaArch.Aggregate(string.Empty, (current, arch) => current + (arch.FullName + Environment.NewLine));
        }    
        
        /// <summary>
        /// Método que permite obtener la lista de las unidades de almacenamiento del equipo
        /// </summary>
        /// <returns>Lista que contiene los nombres de las unidades localizadas</returns>
        public List<string> ObtenerListaUnidadesEquipo()
        {            
            // invoca y deuvelve la consulta de las unidades de los equipos
            return ControladorArchivos.ObtenerListaUnidadesEquipo();
        }
        
        #endregion
        #region Delegados
        #endregion          
        
    }
}
