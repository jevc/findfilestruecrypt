﻿using System.Windows;
using System.Windows.Controls;
using FindFilesTrueCrypt.Ctrl;

namespace FindFilesTrueCrypt
{
               
    public partial class MainWindow
    {
        #region Atributos

        private Controlador _controlador = new Controlador();

        #endregion
        #region Constructores

        /// <summary>
        /// Constructor principal
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            Iniciar();
        }

        #endregion
        #region Propiedades
        #endregion
        #region Metodos

        /// <summary>
        /// Método que permite suscribirse a los eventos de los controles
        /// </summary>
        private void AgregarDelegados()
        {
            btnBuscar.Click += btnBuscar_Click;
        }
        /// <summary>
        /// Método que permite configurar los controles de la aplicación
        /// </summary>
        private void ConfigurarControles()
        {
            txtRutaArchivosLocalizados.TextWrapping = TextWrapping.Wrap;
            txtRutaArchivosLocalizados.AcceptsReturn = true;
            txtRutaArchivosLocalizados.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
        }
        /// <summary>
        /// Método que permite realizar la funcionalidad necesaria al cargar la aplicación
        /// </summary>
        private void Iniciar()
        {
            IniciarControlador();
            LlenarListaUnidades();
            AgregarDelegados();
            ConfigurarControles();            
        }
        /// <summary>
        /// Método que permite iniciar el controlador principal
        /// </summary>
        private void IniciarControlador()
        {
            _controlador = new Controlador();
        }
        /// <summary>
        /// Método que permite cargar la información a visualizar en la lista de Unidades
        /// </summary>
        private void LlenarListaUnidades()
        {
            listaUnidades.ItemsSource = _controlador.ObtenerListaUnidadesEquipo();
        }

        #endregion
        #region Delegados

        /// <summary>
        /// Delegado que se ejecuta al seleccionar la opción de buscar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            // obtiene los archivos sin extensión y los muestra en la caja de texto
            txtRutaArchivosLocalizados.Text = _controlador.ObtenerArchivosTrueCrypt(listaUnidades.Text);
        }

        #endregion
                               
    }
}
